#include <stdio.h>

#include <seq.h>

int main(void){
	track* tr;
	block* bl;
	chunk* ck;
	event* ev;

	seq_init(NULL, NULL, NULL);

	seq_add_track();
	seq_add_track();
	seq_add_track();
	seq_add_track();
	seq_commit();

	seq_walk_tracks();
	tr=seq_next_track();
	tr=seq_next_track();
	tr=seq_next_track();



	ck=seq_mk_chunk();
	seq_insert_block(tr,100,1000,ck);
	seq_commit();

	seq_insert_event(ck,256,0x90,60,128);
	seq_insert_event(ck,512,0x90,60,128);
	seq_commit();
	seq_insert_event(ck,1024,0x90,60,128);
	seq_insert_event(ck,2048,0x90,60,128);
	seq_commit();

	seq_walk_blocks(tr);
	bl=seq_next_block();

	seq_walk_tracks();
	tr=seq_next_track();
	tr=seq_next_track();

	seq_copy_block(bl,tr,300);
	seq_commit();

	seq_walk_events(ck);
	ev=seq_next_event();
	ev=seq_next_event();

	seq_delete_event(ck, ev);
	seq_commit();


	seq_dump();
	

	seq_uninit();

	return 0;
}
