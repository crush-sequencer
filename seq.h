typedef struct track track;
typedef struct block block;
typedef struct chunk chunk;
typedef struct event event;
typedef struct chunk_list chunk_list;



struct track {
	int port; /* read / write */
	int chan; /* read / write */
	block* blocks;
	block* block_ptr;
	struct track* next;
};

struct block {
	unsigned tick; /* read */
	int length; /* read */
	chunk_list* chunks;
	chunk_list* chunk_ptr;
	event* event_ptr;
	struct block* next;
};

struct chunk {
	int r, g, b; /* read / write */
	int ref_c;
	event* events;
};

struct chunk_list {
	chunk* ptr;
	struct chunk_list* next;
};

struct event {
	unsigned tick; /* read */
	int type; /* read */
	int u, v; /* read */
	struct event* next;
};


/* play / stop */
void seq_play(int on_off);
unsigned seq_poll(void);
void seq_seek(unsigned tick);
void seq_reset(void);
void seq_record(int on_off);
void seq_set_record_track(int n);/* FIXME */
void seq_loop(int on_off);
void seq_loop_limits(unsigned tick1, unsigned tick2);
void seq_time_config(unsigned sr, unsigned tpb, unsigned bpm);
void seq_all_notes_off(void);

/* */
void seq_init(void* (*aloc)(size_t), void (*fr)(void*), int (*oom)(void));
void seq_uninit(void);
int seq_undo(void);
int seq_redo(void);
void seq_commit(void);
void seq_clear_all(void);
void seq_instant(track* tr, int type, int u, int v);
chunk* seq_mk_chunk(void);

void seq_dump(void);


/* these operations can be undone */
void seq_add_track(void);
void seq_delete_track(track* tr);
void seq_insert_block(track* tr, unsigned tick, unsigned length, chunk* ck);
void seq_copy_block(block* bl, track* tr, unsigned tick);
void seq_resize_block(block* bl, unsigned length);
void seq_delete_block(track* tr, block* bl);
void seq_push_chunk(block* bl, chunk* ck);
void seq_rrotate_chunk(block* bl);
void seq_lrotate_chunk(block* bl);
void seq_insert_event(chunk* ck, unsigned tick, int type, int u, int v);
void seq_delete_event(chunk* ck, event* ev);
void seq_accept_recording(void);

/* sequencer state reading */
int seq_layer_number(block* bl);
void seq_walk_tracks(void);
track* seq_next_track(void);
void seq_walk_blocks(track* tr);
block* seq_next_block(void);
void seq_walk_events(chunk* ck);
event* seq_next_event(void);

/* audio thread operations */
int seq_advance(unsigned samples, unsigned* used,
	int* port, int* type, int* chan, int* u, int* v);
void seq_record_event(int type, int chan, int u, int v);

/*
sequencer operation

the sequencer is an abstract machine with the following
stateful components.

the sequence itself is a set of tracks, which hold a scheduled 
set of blocks, which hold an order set of event chunks (layers),
each of which holds a scheduled set of midi events. there are 
several raw commands to manipulate the sequence by creating, deleting, 
moving, and inserting the above elements. chunks may be shared among 
many blocks, changes in one block may affect others (because they
point to the same chunk).

the undo history remembers one branch of manipulation commands and
can be used to reverse edits back to the beginning of time. there is
an operation to commit changes so that undo can skip back several
edits at a time. this allows support for implementing more complex
edit operations. the undo history is partially erased every time
you commit.

the play head is a single number that tells the sequencer where it
is in time. there are operations to set the play position. the
play head is naturally advanced using the advance operation, which
also returns the next event to play. when the record operation is
used, an event will be written to the sequence at the current
play head position. when the sequencer is set to play, advance will
actually move the play head and emit events. otherwise advance will
not move the play head and emit no events except those in the instant
queue. see below.

the sequencer has three parameters which are used to control the
real world speed of playback. sample_rate determines the conversion
between samples (used by seq_advance) and real time. ticks_per_beat
determines the conversion between beats (used in tempo) and abstract
tick time units (used by the sequencer). beats_per_week (tempo) determines
the conversion between beats and real time. 1 week is (7*86400) seconds.
the unit of time in almost all the sequencer operations is in ticks.
altering the tempo, sample rate, or ticks per beat during playback may
introduce a one time small time error.

an 'instant queue' contains events will must be scheduled immediately
and will never occur again. there is an operation to enqueue events
for immediate dispatch. as the output mechanism advances the sequencer
play head (using seq_advance) the events in the instant queue will be
dequeued.

an unit which tracks current note ons. when certain operations take
place the sequencer will emit instant events to cancel these events.
this unit is updated when the audio thread advances the play head.

there may be at most two agents which operate on the sequencer, known
as the editor and the audio thread. the audio thread is limited to only
two operations: seq_advance and seq_record_event. both of these operations
have side effects on the sequencer state but are carefully designed to
avoid race conditions and minimize audio thread latency.

*/
