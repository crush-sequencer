#include <stdlib.h>
#include <stdio.h>

#include <seq.h>

#define BUFFER_SIZE 256

typedef struct undo_op undo_op;

struct ring_buffer {
	int front;
	int back;
	struct buffer_slot {
		unsigned p1;
		unsigned p2;
		unsigned p3;
		unsigned p4;
		unsigned p5;
	} buf[BUFFER_SIZE];
};

struct undo_op {
	int commit;
	void (*redo)(undo_op* op);
	void (*undo)(undo_op* op);
	void (*collect)(undo_op* op);
	unsigned p1;
	unsigned p2;
	void* v1;
	void* v2;
	void* v3;
	void* v4;
	struct undo_op* next;
	struct undo_op* prev;
};

struct unstick_node {
	int port;
	int chan;
	int note;
	struct unstick_node* next;
};

enum seq_state {
	SEQ_STOP,
	SEQ_PLAY
};

enum {
	NOTE_ON = 0x90,
	NOTE_OFF = 0x80,
	CONTROLLER = 0xB0,
	PROGRAM = 0xC0,
};


/* internal procedures */
static int default_out_of_memory(void);
static void* default_malloc(size_t s);
static void default_free(void* z);
static void* qmalloc(size_t s);
static void qfree(void* z);

/*
static void emergency_cutoff(void);
static void chan_cutoff(int chan);
static void selective_cutoff(int chan, int note);
*/

static void init_buffer(struct ring_buffer* b);
static void write_buffer(struct ring_buffer* b, unsigned p1, unsigned p2, unsigned p3, unsigned p4, unsigned p5);
static int read_buffer(struct ring_buffer* b, unsigned* p1, unsigned* p2, unsigned* p3, unsigned* p4, unsigned* p5);

static chunk_list* mk_chunk_list(chunk* ck);
static chunk_list* chunk_list_copy(chunk_list* ls);
static void chunk_refup(chunk* ck);
static void chunk_refdown(chunk* ck);
static int rfind_track(track* tr);
static track* find_track(int n);
static chunk* find_chunk_by_tick(track* tr, unsigned tick);
static track* mk_track(void);
static block* mk_block(unsigned tick, unsigned length, chunk* ck);

static void error(const char* msg);

static void move_seq_pointers(unsigned tick);

static event* mk_event(unsigned tick, int type, int u, int v);


static void advance_play_head(unsigned samples);
static int core_sequence(
	track** tr_out,event** ev_out, unsigned* samp_out,
	unsigned N, unsigned D, unsigned sample_limit
);
static int calc_next_event(event**, block**, track**, unsigned*);
static void dispatch_event(
	int* port, int* type, int* chan, int* u, int* v,
	int port_, int type_, int chan_, int u_, int v_
);

static undo_op* mk_undo_op();
static void dummy_undo(undo_op* op);
static void undo_collect(void);
static void undo_push(undo_op* op);

static void redo_add_track(undo_op* op);
static void undo_add_track(undo_op* op);
static void collect_add_track(undo_op* op);

static void redo_delete_track(undo_op* op);
static void undo_delete_track(undo_op* op);
static void collect_delete_track(undo_op* op);

static void redo_insert_block(undo_op* op);
static void undo_insert_block(undo_op* op);
static void collect_insert_block(undo_op* op);

static void redo_resize_block(undo_op* op);
static void undo_resize_block(undo_op* op);
static void collect_resize_block(undo_op* op);

static void redo_delete_block(undo_op* op);
static void undo_delete_block(undo_op* op);
static void collect_delete_block(undo_op* op);

static void redo_push_chunk(undo_op* op);
static void undo_push_chunk(undo_op* op);
static void collect_push_chunk(undo_op* op);

static void redo_rrotate_chunk(undo_op* op);
static void undo_rrotate_chunk(undo_op* op);
static void collect_rrotate_chunk(undo_op* op);

static void redo_lrotate_chunk(undo_op* op);
static void undo_lrotate_chunk(undo_op* op);
static void collect_lrotate_chunk(undo_op* op);

static void redo_insert_event(undo_op* op);
static void undo_insert_event(undo_op* op);
static void collect_insert_event(undo_op* op);

static void redo_delete_event(undo_op* op);
static void undo_delete_event(undo_op* op);
static void collect_delete_event(undo_op* op);


static void stick_note(int port, int chan, int note);
static void unstick_note(int port, int chan, int note);
static int unstick_all(int* port, int* chan, int* note);


/* sequencer state */
static struct ring_buffer in_buf; /* recording */
static struct ring_buffer out_buf; /* instant queue */
static track* the_tracks = NULL;
static unsigned tick_now = 0;
static unsigned tick_err = 0;
static unsigned sample_rate = 44100;
static unsigned ticks_per_beat = 384;
static unsigned beats_per_minute = 120;
static struct undo_op* undo_stack = NULL;
static struct undo_op* undo_ptr = NULL;
static enum seq_state state = SEQ_STOP;
static unsigned loop_begin = 0;
static unsigned loop_end = 384 * 2 * 4;
static unsigned loop_flag = 0;
static int record_flag = 0;
static int rec_track_n = 0;
static track* record_track = NULL;

static track* glbl_track_itor = NULL;
static block* glbl_block_itor = NULL;
static event* glbl_event_itor = NULL;

static void* (*malloc_ptr)(size_t s);
static void (*free_ptr)(void* z);
static int (*out_of_memory_ptr)(void);

static unsigned char emergency_storage[256];

static struct unstick_node* unstick_frees;
static struct unstick_node* unstick_stuck;
static int unstick_flag = 0;




void seq_dump(void){
	track* tr = NULL;
	block* bl = NULL;
	chunk* ck = NULL;
	chunk_list* cl = NULL;
	event* ev = NULL;
	int tr_i;
	int ck_i;

	undo_op* un = NULL;
	int undo_i = 0;


	printf("crush sequencer\n\n");


	printf("undo stack:\n");
	un=undo_stack->next;
	while(un){
		printf("un %p: %p %p\n", un, un->v1, un->v2);
		un=un->next;
	}
	if(undo_ptr==undo_stack) {
		printf("unptr: root\n");
	}
	else {
		printf("unptr: %p\n", undo_ptr);
	}


	printf("sequence:\n");
	tr=the_tracks->next;
	tr_i=0;
	if(tr==NULL){ printf(" nothing\n"); }
	else while(tr) {
		printf("%d track:\n", tr_i++);
		bl=tr->blocks->next;
		while(bl) {
			printf(" %d block (length %d):\n", bl->tick, bl->length);
			cl = bl->chunks;
			ck_i = 0;
			while(cl) {
				ck = cl->ptr;
				ev = ck->events->next;
				if(bl->chunk_ptr == cl){
					printf(" +%d chunk:\n", ck_i++);
				}
				else{
					printf("  %d chunk:\n", ck_i++);
				}
				while(ev) {
					printf("   %d (0x%02x,%d,%d)\n", ev->tick, ev->type, ev->u, ev->v);
					ev=ev->next;
				}
				cl=cl->next;
			}
			bl=bl->next;
		}
		tr=tr->next;
	}
	printf("\n");

	printf("alloc routine = %s\n", malloc_ptr == default_malloc ? "default" : "custom");
	printf("out of memory routine = %s\n", out_of_memory_ptr == default_out_of_memory ? "default" : "custom");
	printf("free routine = %s\n\n", free_ptr == default_free ? "default" : "custom");

	printf("in buffer = \n");
	printf("out buffer = \n\n");

	printf("now = %u\n", tick_now);
	printf("err = %u\n", tick_err);
	printf("srate = %u\n", sample_rate);
	printf("tpb = %u\n", ticks_per_beat);
	printf("bpm = %u\n", beats_per_minute);
	printf("state = %s\n", state==SEQ_PLAY ? "PLAY" : "STOP");
	printf("loop begin = %u\n", loop_begin);
	printf("loop end = %u\n", loop_end);
	printf("looping = %u\n", loop_flag);
	printf("recording = %u\n", record_flag);
	printf("rec track = %u\n", rec_track_n);
	printf("unstick = %u\n\n", unstick_flag);
}

/* play / stop */
void seq_play(int on_off){
	if(on_off){
		state = SEQ_PLAY;
	}
	else{
		state = SEQ_STOP;
	}
}

unsigned seq_poll(void){
	return tick_now;
}

void seq_seek(unsigned tick){
	if(tick > tick_now){
		move_seq_pointers(tick);
		tick_now = tick;
	}
	else{
		tick_now = tick;
		move_seq_pointers(tick);
	}
	seq_all_notes_off();
}

void seq_reset(void){
	seq_seek(0);
}

void seq_record(int on_off){
	record_flag = on_off;
}

void seq_loop(int on_off){
	loop_flag = on_off;
}

void seq_loop_limits(unsigned tick1, unsigned tick2){
	loop_begin = tick1;
	loop_end = tick2;
}

void seq_time_config(unsigned sr, unsigned tpb, unsigned bpm){
	sample_rate = sr;
	ticks_per_beat = tpb;
	beats_per_minute = bpm;
}

void seq_all_notes_off(void){
	unstick_flag = 1;
}

/* */
void seq_init(void* (*aloc)(size_t), void (*fr)(void*), int (*oom)(void)){
	init_buffer(&in_buf);
	init_buffer(&out_buf);

	out_of_memory_ptr = oom ? oom : default_out_of_memory;
	malloc_ptr = aloc ? aloc : default_malloc;
	free_ptr = fr ? fr : default_free;

	the_tracks = mk_track();
	tick_now = 0;
	tick_err = 0;
	sample_rate = 44100;
	ticks_per_beat = 384;
	beats_per_minute = 120;
	undo_stack = mk_undo_op();
	undo_ptr = undo_stack;
	state = SEQ_STOP;
	loop_begin = 0;
	loop_end = 384 * 4 * 2;
	loop_flag = 0;
	record_track = NULL;
	record_flag = 0;
}

void seq_uninit(void){
	seq_clear_all();
	qfree(the_tracks->blocks->chunks);
	qfree(the_tracks->blocks);
	qfree(the_tracks);
	qfree(undo_stack);
}

int seq_undo(void){
	if(undo_ptr == undo_stack){
		return -1;
	}
	while(1){
		if(undo_ptr == undo_stack) break;
		undo_ptr->undo(undo_ptr);
		undo_ptr = undo_ptr->prev;
		if(undo_ptr->commit == 1) break;
	}
	return 0;
}

int seq_redo(void){
	if(undo_ptr->next == NULL){
		return -1;
	}
	while(1){
		undo_ptr = undo_ptr->next;
		if(undo_ptr->commit) break;
		undo_ptr->redo(undo_ptr);
		if(undo_ptr->next == NULL) break;
	}
	return 0;
}

void seq_commit(void){
	undo_op* op = mk_undo_op();
	op->commit = 1;
	undo_push(op);
}

void seq_clear_all(void){
	track* tr;
	block* bl;
	chunk* ck;
	chunk_list* cl;
	event* ev;
	undo_op* un;
	void* next;

	seq_play(0);
	seq_reset();
	seq_all_notes_off();

	/* undo everything (simple memory managment) :( */
	while(seq_undo() == 0)
		;

	/* could be avoided with an automatic memory manager.
	but might introduce just as much interface latency.
	at least here it happens only when you clear all. */

	/* clear future undo stack */
	undo_collect();

	/* then clear remaining structures directly */
/*	tr = the_tracks->next;
	while(tr){
		bl = tr->blocks->next;
		while(bl){
			cl = bl->chunks;
			while(cl){
				if(cl->ptr->ref_c == 1){
					ev = cl->ptr->events;
					while(ev){
						next = ev->next;
						qfree(ev);
						ev = next;
					}
					qfree(cl->ptr);
				}
				else{
					cl->ptr->ref_c -= 1;
				}
				next = cl->next;
				qfree(cl);
				cl = next;
			}
			next = bl->next;
			qfree(bl);
			bl = next;
		}
		next = tr->next;
		qfree(tr->blocks->chunks);
		qfree(tr->blocks);
		qfree(tr);
		tr = next;
	}

	un = undo_stack->next;
	while(un){
		next = un->next;
		qfree(un);
		un = next;
	}
	undo_stack->next = NULL;
	undo_ptr = undo_stack;
*/
	the_tracks->next = NULL;
}

void seq_instant(track* tr, int type, int u, int v){
	int tr_number = rfind_track(tr);
	write_buffer(&out_buf, tr->port, type, tr->chan, u, v);
}

chunk* seq_mk_chunk(void){
	chunk* ck = qmalloc(sizeof(chunk));
	ck->r = 255;
	ck->g = 255;
	ck->b = 255;
	ck->ref_c = 0;
	ck->events = mk_event(0,0,0,0);
	return ck;
}


/* these operations can be undone */
void seq_add_track(void){
	track* tr = mk_track();
	track* ptr = the_tracks;
	undo_op* op;
	while(ptr->next != NULL){
		ptr = ptr->next;
	}

	op = mk_undo_op();
	op->v1 = tr;
	op->v2 = ptr;
	op->redo = redo_add_track;
	op->undo = undo_add_track;
	op->collect = collect_add_track;
	undo_push(op);
}

void seq_delete_track(track* tr){
	track* ptr = the_tracks;
	undo_op* op;
	while(ptr->next && ptr->next != tr){
		ptr = ptr->next;
	}
	if(ptr->next == NULL){
		fprintf(stderr, "delete: track note found\n");
		return;
	}

	op = mk_undo_op();
	op->v1 = tr;
	op->v2 = ptr;
	op->redo = redo_delete_track;
	op->undo = undo_delete_track;
	op->collect = collect_delete_track;
	undo_push(op);
}

void seq_insert_block(track* tr, unsigned tick, unsigned length, chunk* ck){
	block* ptr = tr->blocks;
	block* bl = mk_block(tick, length, ck);
	undo_op* op = mk_undo_op();
	while(ptr->next && ptr->next->tick < tick){
		ptr = ptr->next;
	}

	op->v1 = bl;
	op->v2 = ptr;
	op->redo = redo_insert_block;
	op->undo = undo_insert_block;
	op->collect = collect_insert_block;
	chunk_refup(ck);
	undo_push(op);
}

void seq_delete_block(track* tr, block* bl){
	block* pred = tr->blocks;
	undo_op* op = mk_undo_op();

	while(pred->next && pred->next != bl){
		pred = pred->next;
	}
	if(pred->next == NULL){
		fprintf(stderr, "delete: block not found\n");
		return;
	}

	op->v1 = bl;
	op->v2 = pred;
	op->redo = redo_delete_block;
	op->undo = undo_delete_block;
	op->collect = collect_delete_block;
	undo_push(op);
}

void seq_copy_block(block* bl, track* tr, unsigned tick){
	block* ptr = tr->blocks;
	block* cpy = mk_block(tick, bl->length, NULL);
	undo_op* op = mk_undo_op();
	while(ptr->next && ptr->next->tick < tick){
		ptr = ptr->next;
	}

	qfree(cpy->chunks);//undo part of construction
	cpy->length = bl->length;
	cpy->chunks = chunk_list_copy(bl->chunks);
	cpy->chunk_ptr = cpy->chunks;
	cpy->event_ptr = cpy->chunk_ptr->ptr->events->next;

	op->v1 = cpy;
	op->v2 = ptr;
	/* we can utilize the insert operations here */
	op->redo = redo_insert_block;
	op->undo = undo_insert_block;
	op->collect = collect_insert_block;
	undo_push(op);
}

void seq_resize_block(block* bl, unsigned length){
	undo_op* op = mk_undo_op();
	op->v1 = bl;
	op->p1 = bl->length;
	op->p2 = length;
	op->redo = redo_resize_block;
	op->undo = undo_resize_block;
	op->collect = collect_resize_block;
	undo_push(op);
}

void seq_push_chunk(block* bl, chunk* ck){
	chunk_list* ptr = bl->chunks;
	undo_op* op = mk_undo_op();
	unsigned i;

	op->v1 = mk_chunk_list(ck);
	while(ptr->next != NULL){
		ptr = ptr->next;
	}
	op->v2 = ptr;
	op->v3 = bl;
	op->v4 = bl->chunk_ptr;
	op->redo = redo_push_chunk;
	op->undo = undo_push_chunk;
	op->collect = collect_push_chunk;
	chunk_refup(ck);
	undo_push(op);
}

void seq_rrotate_chunk(block* bl){
	undo_op* op = mk_undo_op();
	op->v1 = bl;
	op->redo = redo_rrotate_chunk;
	op->undo = undo_rrotate_chunk;
	op->collect = collect_rrotate_chunk;
	undo_push(op);
}

void seq_lrotate_chunk(block* bl){
	undo_op* op = mk_undo_op();
	op->v1 = bl;
	op->redo = redo_lrotate_chunk;
	op->undo = undo_lrotate_chunk;
	op->collect = collect_lrotate_chunk;
	undo_push(op);
}

void seq_insert_event(chunk* ck, unsigned tick, int type, int u, int v){
	event* ev = mk_event(tick, type, u, v);
	event* ptr = ck->events;
	undo_op* op;

	while(ptr->next && ptr->next->tick < tick){
		ptr = ptr->next;
	}

	op = mk_undo_op();
	op->v1 = ev;
	op->v2 = ptr;
	op->redo = redo_insert_event;
	op->undo = undo_insert_event;
	op->collect = collect_insert_event;
	undo_push(op);
}

void seq_delete_event(chunk* ck, event* ev){
	event* ptr = ck->events;
	undo_op* op;
	while(ptr->next && ptr->next != ev){
		ptr = ptr->next;
	}
	if(ptr->next == NULL){
		fprintf(stderr, "delete: event not found\n");
		return;
	}

	op = mk_undo_op();
	op->v1 = ev;
	op->v2 = ptr;
	op->redo = redo_delete_event;
	op->undo = undo_delete_event;
	op->collect = collect_delete_event;
	undo_push(op);
}

void seq_accept_recording(void){
	unsigned tick, type, u, v, unused;
	chunk* ck;
	track* record_track = find_track(rec_track_n);

	if(record_track == NULL) return;

	while(!read_buffer(&in_buf, &tick, &type, &u, &v, &unused)){
//		ck = find_chunk_by_tick(record_track, tick);
//		seq_insert_event(ck, tick, type, u, v);
/*
find chunk at tick on track
if exists, insert event
else we need new mechanisms for possibly creating or extending blocks
*/
	}
}

/* sequencer state reading */
int seq_layer_number(block* bl){
	int i = 0;
	chunk_list* ptr = bl->chunks->next;
	while(ptr && ptr != bl->chunk_ptr){
		i = i+1;
		ptr = ptr->next;
	}
	return i;
}

void seq_walk_tracks(void){
	glbl_track_itor = the_tracks->next;
}

track* seq_next_track(void){
	track* ptr = glbl_track_itor;
	if(glbl_track_itor == NULL){
		return NULL;
	}
	else{
		glbl_track_itor = glbl_track_itor->next;
		return ptr;
	}
}

void seq_walk_blocks(track* tr){
	glbl_block_itor = tr->blocks->next;
}

block* seq_next_block(void){
	block* ptr = glbl_block_itor;
	if(glbl_block_itor == NULL){
		return NULL;
	}
	else{
		glbl_block_itor = glbl_block_itor->next;
		return ptr;
	}
}

void seq_walk_events(chunk* ck){
	glbl_event_itor = ck->events->next;
}

event* seq_next_event(void){
	event* ptr = glbl_event_itor;
	if(glbl_event_itor == NULL){
		return NULL;
	}
	else{
		glbl_event_itor = glbl_event_itor->next;
		return ptr;
	}
}




/* audio thread operations */
int seq_advance(
unsigned samples,
unsigned* used,
int* port,
int* type,
int* chan,
int* u,
int* v
){
	unsigned N = beats_per_minute * ticks_per_beat;
	unsigned D = sample_rate * 60;
	unsigned a, b, c, d, e;

	track* tr;
	event* ev;
	unsigned samp_out;

	if(unstick_flag){ /* check for emergency cutoff */
/*		if(unstick_all(port, chan, u)){
			*used = 0;
			*type = NOTE_OFF;
			return 1;
		}
		else{
			unstick_flag = 0;
		}*/
		unstick_flag = 0;
	}
	
	if(read_buffer(&out_buf, &a, &b, &c, &d, &e)){
		/* do immediate events */
		*used = 0;
		dispatch_event(port, type, chan, u, v,
			a, b, c, d, e);
		return 1;
	}


	/* advance to the next event, possibly looping */
	/* return 0 if no event, 1 otherwise */
	if(core_sequence(&tr, &ev, &samp_out, N, D, samples) == 1){
		*used = samp_out;
		dispatch_event(port, type, chan, u, v,
			tr->port, ev->type, tr->chan, ev->u, ev->v);
		return 1;
	}
	else{
		*used = samples;
		return 0;
	}

}


static void advance_play_head(unsigned samples){

}


static int core_sequence(
track** tr_out,
event** ev_out,
unsigned* samp_out,
unsigned N,
unsigned D,
unsigned sample_limit
){
	/* 
get next event, or nothing, or loop end

nothing - return 0
event - if within sample_limit, advance and return 1
loop - if within sample_limit, advance, reset and return core_sequence
	*/


	track* tr;
	block* bl;
	chunk* ck;
	event* ev;
	unsigned tick;
	unsigned samples;
	unsigned now = tick_now;
	int kind = calc_next_event(&ev, &bl, &tr, &tick);

	if(kind == 0){ /* nothing */
		advance_play_head(sample_limit);
		return 0;
	}
	else if(kind == 1){ /* event */
		samples = (tick - now) * D / N;
		advance_play_head(samples);
		*tr_out = tr;
		*ev_out = ev;

		bl->event_ptr = ev->next;
		if(ev->next == NULL){
			tr->block_ptr = bl->next;
			ck = tr->block_ptr->chunk_ptr->ptr;
			tr->block_ptr->event_ptr = ck->events->next;
		}

		return 1;
	}
	else if(kind == 2){ /* loop endpoint */
		samples = (tick - now) * D / N;
		advance_play_head(samples);
		//move to loop start
		error("core sequence");
		return 0;
		//return core_sequence(...);
	}
	else {
		error("core_sequence: ???");
		return 0;
	}
}



void seq_record_event(int type, int chan, int u, int v){
	int track_num = rec_track_n;
	unsigned tick = seq_poll();
	write_buffer(&in_buf, tick, type, u, v, 0);
}




/* internal procedures */
static int default_out_of_memory(void){
	fprintf(stderr, "OUT OF MEMORY (sequencer)");
	exit(13);
}

static void* default_malloc(size_t s){
	return malloc(s);
}

static void default_free(void* z){
	free(z);
}

static void* qmalloc(size_t s){
	void* ptr = malloc_ptr(s);
	if(ptr == NULL){
		if(out_of_memory_ptr() == 0){
			/* recovered */
			return qmalloc(s);
		}
		else{
			fprintf(
			stderr,
			"sequencer unable to recover from out of memory\n"
			);
			return emergency_storage;
		}
	}
	else{
		return ptr;
	}
}

static void qfree(void* z){
	free_ptr(z);
}

static void init_buffer(struct ring_buffer* b){
	b->front = 0;
	b->back = 0;
}

static void write_buffer(
struct ring_buffer* b,
unsigned p1,
unsigned p2,
unsigned p3,
unsigned p4,
unsigned p5
){
	struct buffer_slot s = {p1, p2, p3, p4, p5};
	int f = b->front;
	int k = b->back;
	if(f - k == 1 || k+BUFFER_SIZE - f == 1){
		return;
	}
	else{
		b->buf[k] = s;
		b->back = (k + 1) % BUFFER_SIZE;
	}
}

static int read_buffer(
struct ring_buffer* b,
unsigned* p1,
unsigned* p2,
unsigned* p3,
unsigned* p4,
unsigned* p5
){
	int k = b->back;
	int f = b->front;
	if(f == k){
		return 0;
	}
	else{
		*p1 = b->buf[f].p1;
		*p2 = b->buf[f].p2;
		*p3 = b->buf[f].p3;
		*p4 = b->buf[f].p4;
		*p5 = b->buf[f].p5;

		b->front = (f + 1) % BUFFER_SIZE;
		return 1;
	}
}

static chunk_list* mk_chunk_list(chunk* ck){
	chunk_list* ls = qmalloc(sizeof(chunk_list));
	ls->next = NULL;
	ls->ptr = ck;
	return ls;
}

static chunk_list* chunk_list_copy(chunk_list* ls){
	chunk_list* ptr = ls;
	chunk_list* ret = mk_chunk_list(ptr->ptr);
	chunk_list* ptr2 = ret;

	chunk_refup(ptr->ptr);
	ptr = ptr->next;
	while(ptr){
		ptr2->next = mk_chunk_list(ptr->ptr);
		chunk_refup(ptr->ptr);
		ptr = ptr->next;
		ptr2 = ptr2->next;
	}
	return ret;
}

static track* mk_track(void){
	track* ptr = qmalloc(sizeof(track));
	ptr->port = 0;
	ptr->chan = 0;
	ptr->blocks = mk_block(0, 0, NULL);
	ptr->block_ptr = NULL;
	ptr->next = NULL;
	return ptr;
}

static block* mk_block(unsigned tick, unsigned length, chunk* ck){
	block* ptr = qmalloc(sizeof(block));
	ptr->tick = tick;
	ptr->length = length;
	ptr->event_ptr = NULL;
	ptr->next = NULL;
	ptr->chunks = mk_chunk_list(ck);
	ptr->chunk_ptr = ptr->chunks;
	return ptr;
}



static void chunk_refup(chunk* ck){
	ck->ref_c += 1;
}

static void chunk_refdown(chunk* ck){
	event* ptr = ck->events;
	event* next = NULL;

	ck->ref_c -= 1;
	if(ck->ref_c > 0) return;
	
	qfree(ck->events);
	qfree(ck);
}

static int rfind_track(track* tr){
	track* ptr = the_tracks->next;
	int i = 0;
	while(ptr){
		if(ptr == tr) return i;
		ptr = ptr->next;
	}
	return -1;
}

static track* find_track(int n){
	/* FIXME */
	error("find_track?");
	return NULL;
}

static chunk* find_chunk_by_tick(track* tr, unsigned tick){
	/* TODO */
	error("find_chunk_by_tick?");
	return NULL;
}



static void move_seq_pointers(unsigned tick){
	
}


static event* mk_event(unsigned tick, int type, int u, int v){
	event* ev = qmalloc(sizeof(event));
	ev->tick = tick;
	ev->type = type;
	ev->u = u;
	ev->v = v;
	ev->next = NULL;
	return ev;
}

static undo_op* mk_undo_op(){
	undo_op* op = qmalloc(sizeof(undo_op));
	op->commit = 0;
	op->next = NULL;
	op->prev = undo_ptr;
	op->redo = dummy_undo;
	op->undo = dummy_undo;
	op->collect = dummy_undo;
	op->p1 = 0;
	op->p2 = 0;
	op->v1 = NULL;
	op->v2 = NULL;
	op->v3 = NULL;
	return op;
}

static void dummy_undo(undo_op* op){
	/* no effect */
}

static void undo_collect(){
	undo_op* ptr = undo_ptr;
	undo_op* prev = NULL;
	while(ptr->next != NULL){
		ptr = ptr->next;
	}
	while(ptr != undo_ptr){
		prev = ptr->prev;
		ptr->collect(ptr);
		qfree(ptr);
		ptr = prev;
	}
	undo_ptr->next = NULL;
}

static void undo_push(undo_op* op){
	undo_collect();
	undo_ptr->next = op;
	seq_redo();
}



/*                                    */
/* editting and uneditting procedures */
/*                                    */

static void redo_add_track(undo_op* op){
	track* tr = op->v1;
	track* pred = op->v2;
	if(pred->next != NULL){
		fprintf(stderr, "add track inconsistent\n");
		return;
	}
	pred->next = tr;
}

static void undo_add_track(undo_op* op){
	track* tr = op->v1;
	track* pred = op->v2;
	if(tr->next != NULL){
		fprintf(stderr, "undo add track inconsistent\n");
		return;
	}
	pred->next = NULL;
}

static void collect_add_track(undo_op* op){
	track* tr = op->v1;
	qfree(tr->blocks->chunks);
	qfree(tr->blocks);
	qfree(tr);
}

static void redo_delete_track(undo_op* op){
	track* pred = op->v2;
	pred->next = pred->next->next;
}

static void undo_delete_track(undo_op* op){
	track* tr = op->v1;
	track* pred = op->v2;
	tr->next = pred->next;
	pred->next = tr;
}

static void collect_delete_track(undo_op* op){
	/* no effect */
	/* proof: either the corresponding create op
	 * has been collected or it will not be collected.
	 * if yes, then the track has been freed.
	 * if no, the track is still there and should not be freed.
	 * */
}

static void redo_insert_block(undo_op* op){
	block* bl = op->v1;
	block* pred = op->v2;

	bl->next = pred->next;
	pred->next = bl;
}

static void undo_insert_block(undo_op* op){
	block* pred = op->v2;
	pred->next = pred->next->next;
}

static void collect_insert_block(undo_op* op){
	block* bl = op->v1;
	chunk_list* ptr = bl->chunks;
	chunk_list* next;

	while(ptr){
		next = ptr->next;
		chunk_refdown(ptr->ptr);
		qfree(ptr);
		ptr = next;
	}

	qfree(bl);
}


static void redo_resize_block(undo_op* op){
	block* bl = op->v1;
	unsigned l2 = op->p2;
	bl->length = l2;
}

static void undo_resize_block(undo_op* op){
	block* bl = op->v1;
	unsigned l1 = op->p1;
	bl->length = l1;
}

static void collect_resize_block(undo_op* op){
	/* no effect */
}


static void redo_delete_block(undo_op* op){
	block* pred = op->v2;
	block* bl = op->v1;
	pred->next = bl->next;
}

static void undo_delete_block(undo_op* op){
	block* pred = op->v2;
	block* bl = op->v1;
	bl->next = pred->next;
	pred->next = bl;
}

static void collect_delete_block(undo_op* op){
	/* do nothing */
}


static void redo_push_chunk(undo_op* op){
	chunk_list* pred = op->v2;
	chunk_list* cl = op->v1;
	block* bl = op->v3;
	pred->next = cl;
	bl->chunk_ptr = cl;
}

static void undo_push_chunk(undo_op* op){
	chunk_list* pred = op->v2;
	block* bl = op->v3;
	chunk_list* ptr = bl->chunks;

	pred->next = NULL;
	bl->chunk_ptr = op->v4;
}

static void collect_push_chunk(undo_op* op){
	chunk_list* cl = op->v1;
	chunk_refdown(cl->ptr);
	qfree(cl);
}

static void redo_rrotate_chunk(undo_op* op){
	block* bl = op->v1;
	if(bl->chunk_ptr->next == NULL){
		bl->chunk_ptr = bl->chunks;
	}
	else{
		bl->chunk_ptr = bl->chunk_ptr->next;
	}
}

static void undo_rrotate_chunk(undo_op* op){
	block* bl = op->v1;
	chunk_list* cl = bl->chunks;
	if(bl->chunk_ptr == bl->chunks){
		while(cl->next != NULL){
			cl = cl->next;
		}
		bl->chunk_ptr = cl;
	}
	else{
		while(cl->next != bl->chunk_ptr){
			cl = cl->next;
		}
		bl->chunk_ptr = cl;
	}
}

static void collect_rrotate_chunk(undo_op* op){
	/* do nothing */
}

static void redo_lrotate_chunk(undo_op* op){
	undo_rrotate_chunk(op);
}

static void undo_lrotate_chunk(undo_op* op){
	redo_rrotate_chunk(op);
}

static void collect_lrotate_chunk(undo_op* op){
	/* do nothing */
}

static void redo_insert_event(undo_op* op){
	event* ev = op->v1;
	event* pred = op->v2;
	ev->next = pred->next;
	pred->next = ev;
}

static void undo_insert_event(undo_op* op){
	event* pred = op->v2;
	pred->next = pred->next->next;
}

static void collect_insert_event(undo_op* op){
	qfree(op->v1);
}

static void redo_delete_event(undo_op* op){
	event* pred = op->v2;
	pred->next = pred->next->next;
}

static void undo_delete_event(undo_op* op){
	event* ev = op->v1;
	event* pred = op->v2;
	ev->next = pred->next;
	pred->next = ev;
}

static void collect_delete_event(undo_op* op){
	/* no effect */
	/* proof: if a delete event is collected, then
	 * it means the the delete was undone. either the
	 * corresponding create op is being collected or
	 * it isnt. if it is, the event is not there and
	 * cannot be recovered, and is freed. if it isnt
	 * the event is still there and nothing happens.
	 * */
}







static void dispatch_event(
int* port, int* type, int* chan, int* u, int* v,
int port_, int type_, int chan_, int u_, int v_
){
	*port = port_;
	*type = type_;
	*chan = chan_;
	*u = u_;
	*v = v_;

	if(type_ == NOTE_ON){
		stick_note(port_, chan_, u_);
	}
	else if(type_ == NOTE_OFF){
		unstick_note(port_, chan_, u_);
	}
}



static void handle_cutoff(int* port, int* type, int* chan, int* u, int* v){
	//look for any remembered NOTE_ONs
	//write a corresponding port, NOTE_OFF, chan, u
	//to the output pointers.

	//now wheres that data structure
}





static void stick_note(int port, int chan, int note){
	struct unstick_node* ptr = unstick_frees->next;
	if(ptr == NULL){
		return;
	}
	unstick_frees->next = ptr->next;
	ptr->port = port;
	ptr->chan = chan;
	ptr->note = note;
	ptr->next = unstick_stuck->next;
	unstick_stuck->next = ptr;
}

static void unstick_note(int port, int chan, int note){
	struct unstick_node* ptr = unstick_stuck;
	struct unstick_node* found;
	while(ptr->next){
		if(
		ptr->port == port &&
		ptr->chan == chan &&
		ptr->note == note ){
			found = ptr->next;
			ptr->next = found->next;
			found->next = unstick_frees->next;
			unstick_frees->next = found;
			return;
		}
		ptr = ptr->next;
	}
}

static int unstick_all(int* port, int* chan, int* note){
	error("unstick_all not written");
	/*
	struct unstick_node* ptr;
	if(unstick_stuck->next == NULL){
		return 0;
	}
	else{
		*port = ptr->port;
		*chan = ptr->chan;
		*note = ptr->note;
		unstick_stuck->next = ptr->next;
		ptr->next = unstick_frees->next;
		unstick_frees->next = ptr;
		return 1;
	}*/

	return 0;
}


static int calc_next_event(
event** ev_out,
block** bl_out,
track** tr_out,
unsigned* ticks_out
){
	track* tr = the_tracks->next;
	block* bl;
	chunk* ck;
	event* ev_min = NULL;
	event* ev;

	unsigned now = tick_now;

	while(tr){
		bl = tr->block_ptr;
		if(ev_min == NULL
		|| bl->event_ptr->tick + bl->tick < ev_min->tick){
			ev = bl->event_ptr;
			*ev_out = ev;
			*bl_out = bl;
			*tr_out = tr;
			*ticks_out = ev->tick + bl->tick - now;
			ev_min = ev;
		}
		tr = tr->next;
	}
	
	if(ev_min == NULL){
		if(loop_flag){
			*ticks_out = loop_end;
			return 2;
		}
		else{
			return 0;
		}
	}
	else if(loop_flag && loop_end < ev_min->tick + bl->tick){
		*ticks_out = loop_end - now;
		return 2;
	}
	else{
		return 1;
	}

}




static void error(const char* msg){
	fprintf(stderr, msg);
	exit(40);
}
